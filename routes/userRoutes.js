const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers.js");
const cartController = require("../controllers/cartControllers.js");
const tokenController = require("../controllers/tokenControllers.js");
const auth = require("../auth.js");

/* S42 Deliverables - Start */
// User Registration
router.post(
    "/register",
    userController.checkEmailExists,
    userController.checkUsernameExists,
    userController.registerUser,
    cartController.createCart
);

// Set admin role (Admin Only)
router.patch(
    "/role/update",
    auth.verifyToken,
    auth.isAdmin,
    userController.updateRole
);
/* S42 Deliverables - End */

// User Login
router.post(
    "/login", 
    userController.loginUser
);

router.get(
    "/profile",
    // auth.verifyToken,
    // auth.isAdmin,
    userController.getUserDetails
);


// Forgot password
router.post(
    "/password/forgot",
    userController.forgotPassword,
)

/* With Params */
// Reset password
router.post(
    "/password/reset/:token",
    tokenController.checkTokenExists,
    userController.resetPassword,
    tokenController.archiveToken
)

module.exports = router;
