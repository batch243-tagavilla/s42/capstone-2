const express = require("express");
const router = express.Router();
const cartController = require("../controllers/cartControllers.js");
const auth = require("../auth.js");

/* S45 Deliverables - Start */
// Add items to cart (User only)
router.post(
    "/add", 
    auth.verifyToken,
    auth.isRegularUser, 
    cartController.createCart,
    cartController.addToCart
);
/* S45 Deliverables - End */

// Get the cart of the current user (User only)
router.get(
    "/get",
    auth.verifyToken,
    auth.isRegularUser,
    cartController.getCart
)

// Clear the cart of the current user (User Only)
router.get(
    "/clear",
    auth.verifyToken,
    auth.isRegularUser,
    cartController.clearCart
)

module.exports = router;