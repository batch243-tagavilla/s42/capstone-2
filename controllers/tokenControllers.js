const Token = require("../models/Token.js");

module.exports.checkTokenExists = (request, response, next) => {
    const token = request.params.token;
    return Token.findOne({ tokenStr: token })
        .then((result) => {
            if (result) {
                return response.send(`Link is not valid anymore.`);
            } else {
                next();
            }
        })
        .catch((error) => response.status(400).send(error));
};

module.exports.archiveToken = (request, response) => {
    const token = request.params.token;
    return new Token({
        tokenStr: token,
    })
        .save()
        .then(() => {
            response.send('Password has been changed successfully!')
        })
        .catch((error) => response.send(error));
};
