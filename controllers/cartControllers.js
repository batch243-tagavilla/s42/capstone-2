const Cart = require("../models/Cart.js");
const Card = require("../models/Card.js");
const auth = require("../auth.js");
const { get } = require("mongoose");

module.exports.createCart = (request, response, next) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);

    return Cart.findOne({ userId: userData.id })
        .then((result) => {
            if (result !== null) {
                next();
            } else {
                const newCart = new Cart({
                    userId: userData.id,
                });

                return newCart
                    .save()
                    .then(() => {
                        next();
                    })
                    .catch((error) => response.send(error));
            }
        })
        .catch((error) => response.send(error));
};

/* S45 Deliverables - Start */
module.exports.addToCart = async (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);
    const itemQuantity = request.body.quantity;

    const getItem = await Card.findByIdAndUpdate(request.body.cardId)
        .then((result) => {
            let stocks = result.stock;
            if (!result.isActive) {
                // response.send(`This item is not available.`);
                response({isAvailable: false})
                return null;
            } else {
                if (stocks <= 0) {
                    // response.send(`Sorry, no stocks left.`);
                    response({isAvailable: false})
                    return null;
                } else {
                    if (stocks >= itemQuantity) {
                        // result.stock -= itemQuantity;
                        return result;
                        // .save()
                        // .then((result) => {
                        //     return result;
                        // })
                        // .catch((error) => {
                        //     console.log(error);
                        //     response.send(error);
                        // });
                    } else {
                        // response.send(
                        //     `Sorry, only ${stocks} stocks remaining.`
                        // );
                        response({isAvailable: false})
                        return null;
                    }
                }
            }
        })
        .catch((error) => response.send(error));

    const totalPrice = getItem.price * itemQuantity;
    const order = {
        productId: getItem._id,
        productName: getItem.cardName,
        productPrice: getItem.price,
        productQuantity: itemQuantity,
        totalProductPrice: totalPrice,
    };

    const cart = await Cart.findOne({ userId: userData.id })
        .then((result) => {
            return result;
        })
        .catch((error) => response.send(error));

    return Cart.findByIdAndUpdate(cart.id).then((cart) => {
        cart.products.push(order);

        for (let i = cart.products.length - 1; i < cart.products.length; i++) {
            cart.totalAmount += cart.products[i].totalProductPrice;
        }

        return cart
            .save()
            .then((result) => {
                // console.log(result);
                response.send(result);
            })
            .catch((error) => {
                console.log(error);
                response.send(error);
            });
    });
};
/* S45 Deliverables - End */

module.exports.clearCart = async (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);

    const getCartId = await Cart.findOne({ userId: userData.id })
        .then((result) => {
            return result.id;
        })
        .catch((error) => response.send(error));

    return Cart.findByIdAndUpdate(getCartId)
        .then((result) => {
            result.products.splice(0, result.products.length);
            result.totalAmount = 0;

            return result
                .save()
                .then(() => {
                    response.send(
                        // `Checked-out successfully. Cart is now empty`
                        {isCartEmpty: true}
                    );
                })
                .catch((error) => {
                    console.log(error);
                    response.send(error);
                });
        })
        .catch((error) => {
            console.log(error);
            response.send(error);
        });
};

module.exports.getCart = (request, response) => {
    const token = request.headers.authorization;
    const userData = auth.getPayload(token);

    return Cart.findOne({ userId: userData.id })
        .then((result) => {
            if (result === null) {
                // response.send(`Cart is empty.`);
                response.send({isCartEmpty: true})
                return null;
            } else {
                response.send(result);
                return result;
            }
        })
        .catch((error) => response.send(error));
};
