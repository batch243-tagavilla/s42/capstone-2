const Card = require("../models/Card.js");

/* S43 Deliverables - Start */
module.exports.createCard = (request, response) => {
    let newCard = new Card({
        cardName: request.body.cardName,
        description: request.body.description,
        gameName: request.body.gameName,
        price: request.body.price,
        stock: request.body.stock,
        imgSrc: request.body.imgSrc,
    });

    return newCard
        .save()
        .then((card) => {
            console.log(`The following card has been added: \n ${card}`);
            // response.send(
            //     `"${card.cardName}" has been added successfully!\n${card._id}`
            // );
            response.send({ isCardAdded: true });
        })
        .catch((error) => response.send(error));
};

module.exports.getActiveCards = (request, response) => {
    return Card.find({ isActive: true })
        .then((result) => {
            if (result.length > 0) {
                response.send(result);
            } else {
                response.send(`No result.`);
            }
        })
        .catch((err) => {
            response.send(err);
        });
};

module.exports.getAllCards = (request, response) => {
    return Card.find()
        .then((result) => {
            response.send(result);
        })
        .catch((err) => {
            console.log(err);
            response.send(err);
        });
};
/* S43 Deliverables - End */

/* S44 Deliverables - Start */
module.exports.getSpecificCard = (request, response) => {
    const cardId = request.params.cardId;

    return Card.findById(cardId)
        .then((result) => {
            if (result.isActive) {
                response.send(result);
            } else {
                response.send(`No result found`);
            }
        })
        .catch((err) => {
            console.log(err);
            response.send(err);
        });
};

module.exports.archiveCard = (request, response) => {
    const targetCardId = request.body.cardId;
    console.log(request.body.cardId);

    return Card.findById(targetCardId)
        .then((result) => {
            let toggleActive = {
                isActive: !result.isActive,
            };

            return Card.findByIdAndUpdate(targetCardId, toggleActive, {
                new: true,
            })
                .then((card) => {
                    let message = "";
                    card.isActive
                        ? (message = `'${card.cardName}' has been set to active`)
                        : (message = `'${card.cardName}' has been archived`);
                    response.send(message);
                })
                .catch((error) => {
                    response.send(error);
                });
        })
        .catch((err) => {
            response.send(err);
        });
};
/* S44 Deliverables - End */

module.exports.searchCard = (request, response) => {
    const searchStr = request.body.cardName;

    return Card.find({
        cardName: { $regex: searchStr, $options: "i" },
        isActive: { $eq: true },
    })
        .then((result) => {
            if (result.length > 0) {
                response.send(result);
            } else {
                response.send(`No result found.`);
            }
        })
        .catch((err) => {
            response.send(err);
        });
};

module.exports.updateCard = (request, response) => {
    const cardId = request.body.cardId;

    return Card.findByIdAndUpdate(
        cardId,
        {
            cardName: request.body.cardName,
            description: request.body.description,
            gameName: request.body.gameName,
            price: request.body.price,
            stock: request.body.stock,
            isActive: request.body.isActive,
            imgSrc: request.body.imgSrc,
        },
        { new: true, runValidators: true }
    )
        .then((result) => {
            // response.send(result);
            response.send({ isCardUpdated: true });
        })
        .catch((err) => {
            response.send(err);
        });
};

module.exports.getArchiveCards = (request, response) => {
    return Card.find({ isActive: false })
        .then((result) => {
            if (result.length > 0) {
                response.send(result);
            } else {
                response.send(`No result found.`);
            }
        })
        .catch((err) => {
            response.send(err);
        });
};
