const User = require("../models/User.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");

/* S42 Deliverables - Start */
module.exports.registerUser = (request, response, next) => {
    if (request.body.password === "") {
        // return response.send(`Password cannot be blank`);
        return response.send(false);
    } else {
        const newUser = new User({
            username: request.body.username,
            email: request.body.email,
            password: bcrypt.hashSync(request.body.password, 10),
        });

        return newUser
            .save()
            .then((result) => {
                console.log(
                    `The following user has been registered: \n ${result}`
                );
                response.send(
                    // `Congratulations! '${newUser.email}' has been registered.`
                    true
                );
                next();
            })
            .catch((error) => response.send(error));
    }
};

module.exports.checkEmailExists = (request, response, next) => {
    const eMail = request.body.email;
    return User.find({ email: eMail })
        .then((result) => {
            if (result.length > 0) {
                return response.send(
                    // `Email: '${eMail}' has already been registered! Please use another email.`
                    // false
                    {emailExists: true}
                );
            } else {
                next();
            }
        })
        .catch((error) => response.status(400).send(error));
};

module.exports.checkUsernameExists = (request, response, next) => {
    const userName = request.body.username;
    return User.find({ username: userName })
        .then((result) => {
            if (result.length > 0) {
                return response.send(
                    // `Username: '${userName}' has already been used! Please choose another username.`
                    // false
                    {usernameExists: true}
                );
            } else {
                next();
            }
        })
        .catch((error) => response.status(400).send(error));
};

module.exports.updateRole = (request, response) => {
    let targetUserId = request.body.userId;

    return User.findById(targetUserId)
        .then((result) => {
            let toggleAdmin = {
                isAdmin: !result.isAdmin,
            };

            return User.findByIdAndUpdate(targetUserId, toggleAdmin, {
                new: true,
            })
                .then((user) => {
                    let message = "";
                    user.isAdmin
                        ? (message = `Admin access for user '${user.username}' has been granted`)
                        : (message = `Admin access for user '${user.username}' has been revoked`);
                    response.send(message);
                })
                .catch((error) => response.send(error));
        })
        .catch((error) => response.send(error));
};
/* S42 Deliverables - End */

module.exports.loginUser = (request, response) => {
    const eMail = request.body.email;
    const userName = request.body.username;

    return User.findOne({ $or: [{ email: eMail }, { username: userName }] })
        .then((result) => {
            if (result === null) {
                // response.send(`User not found. Register first!`);
                response.send(false);
            } else {
                const isPasswordCorrect = bcrypt.compareSync(
                    request.body.password,
                    result.password
                );

                if (isPasswordCorrect) {
                    let token = auth.createAccessToken(result);
                    return response.send({
                        accessToken: token,
                    });
                } else {
                    return response.send(
                        // `Incorrect password. Please try again.`
                        false
                    );
                }
            }
        })
        .catch((error) => response.send(error));
};

module.exports.getUserDetails = (request, response) => {
    // return User.findById(request.params.userId)
    //     .then((result) => {
    //         result.password = "******";
    //         console.log(result);
    //         return response.send(result);
    //     })
    //     .catch((error) => {
    //         console.log(error);
    //         return response.send(error);
    //     });

    const userData = auth.getPayload(request.headers.authorization);
	console.log(userData);
	
	return User.findById(userData.id).then(result => {
		result.password = "******";
		return response.send(result)
	})
    .catch(err => {
		return response.send(err);
	})
};

module.exports.forgotPassword = (request, response) => {
    return User.findOne({ email: request.body.email })
        .then((result) => {
            if (result) {
                let token = auth.createPasswordToken(result);
                return response.send(
                    `localhost:8080/account/password/reset/${token}`
                );
            } else {
                return response.send(
                    `'${request.body.email}' is not yet registered!`
                );
            }
        })
        .catch((error) => response.status(400).send(error));
};

module.exports.resetPassword = (request, response, next) => {
    const token = request.params.token;
    const userData = auth.decodePasswordToken(token);
    if (userData !== null) {
        const oldPassword = userData.password;
        const newPassword = request.body.newPassword;
        const isPasswordSame = bcrypt.compareSync(newPassword, oldPassword);

        if (isPasswordSame) {
            return response.send(
                `Password should not be the same as your old password`
            );
        } else {
            return User.findByIdAndUpdate(
                userData.id,
                {
                    password: bcrypt.hashSync(newPassword, 10),
                },
                { new: true }
            )
                .then(() => {
                    next();
                })
                .catch((err) => {
                    response.send(err);
                });
        }
    } else {
        return response.send(`Token is not valid`);
    }
};